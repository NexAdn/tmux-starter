#pragma once

#include <string>
#include <tuple>
#include <vector>

#include <ncurses.h>

class Menu
{
	using item_t = std::tuple<std::string, attr_t>;

private:
	WINDOW* win;

	std::vector<item_t> items;

	size_t selected_item{0};

	size_t padding;
	size_t max_label_length;

	constexpr static int ENTER{10};

public:
	Menu(WINDOW* win, size_t padding = 2);
	~Menu();

	size_t add_item(const std::string& label);
	size_t add_item(const std::string& label, const attr_t& attrs);
	size_t add_item(const item_t& item);

	size_t menu_select();

private:
	void draw_menu() const;
	bool can_select(size_t num) const;
};
