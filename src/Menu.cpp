#include <algorithm>

#include <ncurses.h>

#include "Menu.hpp"

Menu::Menu(WINDOW* win, size_t padding) : win(win), padding(padding), max_label_length(2 * padding)
{}

Menu::~Menu()
{}

size_t Menu::add_item(const std::string& label)
{
	return add_item(item_t{label, 0});
}

size_t Menu::add_item(const std::string& label, const attr_t& attrs)
{
	return add_item(item_t{label, attrs});
}

size_t Menu::add_item(const item_t& item)
{
	max_label_length = std::max(max_label_length, std::get<0>(item).length());
	items.push_back(item);
	return items.size() - 1;
}

size_t Menu::menu_select()
{
	draw_menu();
	while (true) {
		int ch = getch();
		switch (ch) {
		case 'k':
		case KEY_UP:
			if (selected_item == 0) {
				selected_item = items.size() - 1;
			} else {
				selected_item = (selected_item - 1);
			}
			draw_menu();
			break;

		case 'j':
		case KEY_DOWN:
			selected_item = (selected_item + 1) % items.size();
			draw_menu();
			break;

		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9': {
			size_t num = ch - 48;
			if (can_select(num - 1)) {
				return num - 1;
			}
		} break;

		case '0':
			if (items.size() > 0) {
				return items.size() - 1;
			}
			break;

		case ENTER:
			return selected_item;
			break;

		default:
			break;
		}
	}
}

void Menu::draw_menu() const
{
	size_t x_off{0};
	size_t y_off{0};
	for (auto& item : items) {
		attr_t original_attrs;
		wattr_get(win, &original_attrs, (short*) nullptr, 0);

		wattrset(win, std::get<1>(item));

		if (y_off == selected_item) {
			wattron(win, A_REVERSE);
		}

		move(y_off, x_off);
		for (size_t i = 0; i < padding; i++) {
			if (i == 0 && padding > 1 && y_off < 9) {
				wprintw(win, std::to_string(y_off + 1).c_str());
			} else if (i == 0 && padding > 1 && y_off == items.size() - 1) {
				wprintw(win, "0");
			} else {
				wprintw(win, " ");
			}
		}

		printw(std::get<0>(item).c_str());
		for (int i = std::get<0>(item).length(); i < max_label_length + padding; i++) {
			wprintw(win, " ");
		}

		if (y_off == selected_item) {
			wattroff(win, A_REVERSE);
		}
		y_off++;

		wattrset(win, original_attrs);
	}
	wrefresh(win);
}

bool Menu::can_select(size_t idx) const
{
	return items.size() > idx;
}
