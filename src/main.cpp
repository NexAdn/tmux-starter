#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

#include <cstdlib>

#include <unistd.h>

#include <ncurses.h>

#include "Menu.hpp"

namespace
{
constexpr const int COLOR_PAIR_ATTACHED{1};
constexpr const int COLOR_PAIR_NORMALSESS{2};
constexpr const int COLOR_PAIR_NOSESS{3};
constexpr const int COLOR_PAIR_ROOTSESS{4};
constexpr const int COLOR_PAIR_ROOT_ATTACHED{5};
constexpr const int COLOR_PAIR_ROOT_NOSESS{6};
} // namespace

std::vector<std::tuple<std::string, attr_t>> get_open_tmux_sessions(bool root_sessions = false)
{
	FILE* fp;
	char cmd_out[1024];

	if (root_sessions) {
		fp = popen("/usr/bin/env sudo -n /usr/bin/env tmux ls", "r");
	} else {
		fp = popen("/usr/bin/env tmux ls", "r");
	}
	if (fp == nullptr) {
		return {};
	}

	std::vector<std::tuple<std::string, attr_t>> session_names;

	while (fgets(cmd_out, sizeof(cmd_out) - 1, fp) != nullptr) {
		std::string line = cmd_out;
		size_t end_idx = line.find_first_of(':');
		std::string session_name = line.substr(0, end_idx);
		attr_t attrs = 0;
		if (line.find("(attached)") != std::string::npos) {
			if (root_sessions) {
				attrs = COLOR_PAIR(COLOR_PAIR_ROOT_ATTACHED);
			} else {
				attrs = COLOR_PAIR(COLOR_PAIR_ATTACHED);
			}
		} else {
			if (root_sessions) {
				attrs = COLOR_PAIR(COLOR_PAIR_ROOTSESS);
			} else {
				attrs = COLOR_PAIR(COLOR_PAIR_NORMALSESS);
			}
		}
		session_names.push_back({session_name, attrs});
	}

	pclose(fp);

	return session_names;
}

int main(int argc, char** argv)
{
	initscr();
	cbreak();
	noecho();
	keypad(stdscr, true);

	if (has_colors()) {
		start_color();

		init_pair(COLOR_PAIR_ATTACHED, COLOR_CYAN, COLOR_BLACK);
		init_pair(COLOR_PAIR_NORMALSESS, COLOR_WHITE, COLOR_BLACK);
		init_pair(COLOR_PAIR_NOSESS, COLOR_GREEN, COLOR_BLACK);
		init_pair(COLOR_PAIR_ROOTSESS, COLOR_WHITE, COLOR_RED);
		init_pair(COLOR_PAIR_ROOT_ATTACHED, COLOR_CYAN, COLOR_RED);
		init_pair(COLOR_PAIR_ROOT_NOSESS, COLOR_GREEN, COLOR_RED);
	}

	auto open_tmux_sessions = get_open_tmux_sessions();
	auto open_tmux_root_sessions = get_open_tmux_sessions(true);

	std::vector<std::tuple<std::string, attr_t>> available_tmux_sessions(
	  open_tmux_sessions.size() + open_tmux_root_sessions.size());

	Menu menu(stdscr);

	for (auto& sess : open_tmux_sessions) {
		menu.add_item(sess);
		available_tmux_sessions.push_back(sess);
	}
	for (auto& sess : open_tmux_root_sessions) {
		menu.add_item(sess);
		available_tmux_sessions.push_back(sess);
	}
	size_t new_session_idx =
	  menu.add_item("-- New session --", A_ITALIC | COLOR_PAIR(COLOR_PAIR_NOSESS));
	size_t new_root_session_idx =
	  menu.add_item("-- New root session --", A_ITALIC | COLOR_PAIR(COLOR_PAIR_ROOT_NOSESS));

	size_t selected_session = menu.menu_select();

	move(0, 0);

	if (selected_session == new_session_idx) {
		endwin();
		printf("Selected new window\n");
		execl("/usr/bin/env", "env", "tmux", "new", nullptr);
	} else if (selected_session == new_root_session_idx) {
		endwin();
		printf("Selected new root window\n");
		execl("/usr/bin/env", "env", "sudo", "-n", "tmux", "new", "-c", "~", nullptr);
	} else if (selected_session < open_tmux_sessions.size()) {
		endwin();
		printf("Attaching to session %lu\n", selected_session);
		execl("/usr/bin/env", "env", "tmux", "attach", "-t",
		      std::get<0>(available_tmux_sessions.at(selected_session)).c_str(), nullptr);
	} else {
		endwin();
		printf("Attaching to root session %lu\n", selected_session - open_tmux_sessions.size());
		execl("/usr/bin/env", "env", "sudo", "-n", "tmux", "attach", "-t",
		      std::get<0>(available_tmux_sessions.at(selected_session)).c_str(), nullptr);
	}
	std::cout << "There was an error whilst starting tmux\nDropping into a normal shell\n";
	execl("/bin/sh", "sh", nullptr);
	std::cout << "Failed to drop into a normal shell\n";

	return 1;
}
