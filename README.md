# tmux-starter

tmux-starter is a simple tool using ncurses to interactively force the user to select a tmux session to work in.

## Dependencies

  * Linux
  * ncurses
  * CMake
  * Make
  * Git

## Building

```
git clone https://gitlab.com/NexAdn/tmux-starter.git
cd tmux-starter
mkdir -p build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
make
```

tmux-starter is then available as executable under `.../src/tmux-starter` in the build directory.

## Installing

As root, after building type:
```
make install
```

## Using tmux-starter

To use tmux-starter, simply run the executable. tmux-starter automatically detects all running tmux sessions and provides a menu to allow selecting an existing tmux session to attach to or create a new session.

## TODOs

  * Add proper installation information to allow `make install`
  * Make the UI less eye-cancer-ish
  * Allow setting a name for newly created tmux sessions inside tmux-starter

## License

Copyright (C) 2019, 2020 Adrian Schollmeyer <git+tmux-starter@nexadn.de>.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
